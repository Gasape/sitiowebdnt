
// Configuracion del firebase
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCvcD7fFWJK0W0BxT6UwU5SkUVZIbbkvcM",
  authDomain: "proyecto-dnt.firebaseapp.com",
  projectId: "proyecto-dnt",
  databaseURL: "https://proyecto-dnt-default-rtdb.firebaseio.com/",
  storageBucket: "proyecto-dnt.appspot.com",
  messagingSenderId: "850154684348",
  appId: "1:850154684348:web:4a1e0fd25388c7178ff5b9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var btnLoguear = document.getElementById('btnlogin');
var btnDisconnect = document.getElementById('btnDesconetar');
var llenado = document.getElementById('llenar');
var comprobar = document.getElementById('comprobarCuenta');
var paginaFULL = document.getElementById('paginaADMIN');

var email = "";
var password = "";

const auth = getAuth();

// leer la informacion para el inicio de sesion
function leer(){
    email = document.getElementById('correo').value;
    password = document.getElementById('pass').value;
}

function comprobarAUTH(){
    onAuthStateChanged(auth, (user) => {
      if (user) {
        alert("Usuario detectado");
        document.getElementById('todo').style.display="block";
        // ...
      } else {
        // User is signed out
        // ...
        alert("No se ha detectado un usuario");
        window.location.href="https://2020030330.netlify.app/html/home";
      }
    });
  }


  if(window.location.href == "https://2020030330.netlify.app/html/admin"){
  window.onload = comprobarAUTH();
    }

if(btnLoguear){
    btnLoguear.addEventListener('click', (e)=>{
      leer();
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in 
          const user = userCredential.user;
          alert("Se inició sesion de manera exitosa");
          window.location.href="https://2020030330.netlify.app/html/admin";
          // ...
        })
        .catch((error) => {
          alert("Datos incorrectos")
          const errorCode = error.code;
          const errorMessage = error.message;
        });
    });
  
  }

  if(btnDisconnect){
    btnDisconnect.addEventListener('click',  (e)=>{
      signOut(auth).then(() => {
        alert("Sesión cerrarda con exito")
        window.location.href="https://2020030330.netlify.app/html/home";
        // Sign-out successful.
      }).catch((error) => {
        // An error happened.
      });
    });
  }
  
  
  // Para crear contenido en la pag de admin
  
  function Rellamar(){
    if(btnDisconnect){
      btnDisconnect.addEventListener('click',  (e)=>{
        signOut(auth).then(() => {
          alert("Sesión cerrarda con exito")
          window.location.href="https://2020030330.netlify.app/html/home";
          // Sign-out successful.
        }).catch((error) => {
          // An error happened.
        });
      });
    }
  
    if(comprobar){
      comprobar.addEventListener('click', (e)=>{
       const user = auth.currentUser;
       if (user !== null) {
         const email = user.email;
         alert(email);
     }
     });
   }
}