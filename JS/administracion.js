// Configuracion del firebase
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { getStorage,ref as refS, uploadBytes,getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js"

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCvcD7fFWJK0W0BxT6UwU5SkUVZIbbkvcM",
  authDomain: "proyecto-dnt.firebaseapp.com",
  projectId: "proyecto-dnt",
  databaseURL: "https://proyecto-dnt-default-rtdb.firebaseio.com/",
  storageBucket: "proyecto-dnt.appspot.com",
  messagingSenderId: "850154684348",
  appId: "1:850154684348:web:4a1e0fd25388c7178ff5b9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

// DeclaraciÃ³n de objetos
var btnInsertar = document.getElementById('btnInsertar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
var btnTodos = document.getElementById('btnTodos');
var btnLimpiar = document.getElementById('btnLimpiar');
var lista = document.getElementById('lista');
var productos = document.getElementById('productos');
var btnMostrarImagen = document.getElementById('verImagen');
var archivo = document.getElementById('archivo');
var ID = "";
var nombre = "";
var descripcion = "";
var precio = "";
var nombreIMG = "";
var url = "";
var estado = "";

if(window.location.href == "https://2020030330.netlify.app/html/catalogo"){
  window.onload = mostrarProductos();
}
if(window.location.href == "http://127.0.0.1:5500/HTML/Catalogo.html"){
    window.onload = mostrarProductos();
}

async function mostrarProductos(){

    const db = getDatabase();
    const dbRef = ref(db, 'productosExample');

    onValue(dbRef, (snapshot) => {
        if(lista){
            lista.innerHTML = "";
        }
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            if(lista){
                if(childData.estado == 0){
                    lista.innerHTML = lista.innerHTML + "<div class='card'> "+ "<img src=' " + childData.url + "'> <h2>" + childData.nombre + "</h2> <p>"  + childData.descripcion + "</p><p> Precio: $" + childData.precio+ " MXN</p>"+ "<p>Estado: " + childData.estado+ "</p><button>COMPRAR (id:"+ childKey + ")</button>";

                }
                else if (childData.estado == 1){
                lista.innerHTML = lista.innerHTML + "<div class='card'> "+ "<img src=' " + childData.url + "'> <h2>" + childData.nombre + "</h2> <p>"  + childData.descripcion + "</p><p> Precio: $" + childData.precio+ " MXN</p>" + "<p>Estado: "+ childData.estado+ "</p><button>COMPRAR (id:"+ childKey + ")</button>" ;
                }
            }else if(productos){
                if(childData.estado == 0){
                    productos.innerHTML = productos.innerHTML + "<div class='card'> "+ "<img src=' " + childData.url + "'> <h2>" + childData.nombre + "</h2> <p>"  + childData.descripcion + "</p><p> Precio: $" + childData.precio+ " MXN</p>" + "<button>COMPRAR</button></div>";
                }
                }
                
        
        });
    },{
        onlyOnce: true
    });

}


function leerInputs(){
    ID = document.getElementById('id').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    nombreIMG = document.getElementById('imgNombre').value;
    url = document.getElementById('url').value;
    estado = document.getElementById('status').value
    
}


async function insertarDatos(){

    await subirImagen();

    leerInputs();

    await insertar();

    await mostrarProductos();  
}

async function insertar(){
    await set(ref(db,'productosExample/' + ID), {
            nombre: nombre,
            descripcion: descripcion,
            precio: precio,
            nombreIMG: nombreIMG,
            url: url,
            estado: estado
    
        }).then((response)=>{
            alert("Se agregó con exito");
        }).catch((error)=>{
            alert("Surgio un error: " + error);
        });
}


async function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);
    
    get(child(dbref,'productosExample/' + ID)).then((snapshot)=>{
        if(snapshot.exists()) {
            nombre = snapshot.val().nombre;
            descripcion = snapshot.val().descripcion;
            precio = snapshot.val().precio;
            nombreIMG = snapshot.val().nombreIMG;
            url = snapshot.val().url;
            estado = snapshot.val().url;

            escribirInputs();
        }else{
            alert("No existe el producto");
        }
    }).catch((error)=>{
        alert("Surgió un error: " + error);
    });
}

function escribirInputs(){
    document.getElementById('id').value = ID;
    document.getElementById('nombre').value = nombre;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('precio').value = precio;
    document.getElementById('imgNombre').value = nombreIMG;
    document.getElementById('url').value = url;
    document.getElementById('status').value = estado;

}

async function actualizar(){
    await subirImagen();

    leerInputs();

    await conseguir();

}


async function conseguir(){

        update(ref(db,'productosExample/'+ ID),{
            nombre:nombre,
            descripcion:descripcion,
            precio:precio,
            nombreIMG: nombreIMG,
            url: url,
            estado: estado
    
        }).then(()=>{
            alert("Se actualizó correctamente");
            mostrarProductos();
        }).catch(()=>{
            alert("Ocurrio un error: " + error);
        });
  
}

function borrar(){

    leerInputs();

    if (ID == "") {
        alert("No hay ID que coincida con el producto");
        return;
    }else if(ID != null){
        remove(ref(db,'productosExample/'+ ID))
        .then(()=>{
            alert("Se elimino el registro correctamente");
            mostrarProductos();
    }).catch(()=>{
        alert("Ocurrio un error: " + error);
    });
    }

}

function limpiar(){

    lista.innerHTML="";
    ID = "";
    nombre = "";
    descripcion = "";
    precio = "";
    nombreIMG = "";
    url = "";
    estado = "0";
    
    escribirInputs();

}

var file = "";
var name = "";

// Permite cargar la imagen
function cargarImagen(){

    // archivo seleccionado
    file = event.target.files[0];
    name = event.target.files[0].name;
    document.getElementById('imgNombre').value = name;
}

// Llama la funciÃ³n cargar imagen y sube a la nube la imagen cargada
async function subirImagen(){
    // Sirve para subir la imagen al STORAGE

    document.getElementById('imgNombre').value = name;
    const storageRef = refS(storage, 'Imagenes/' + name);

    // 'file' comes from the Blob or File API
    await uploadBytes(storageRef, file).then((snapshot) => {
        alert("Se cargo el archivo");
    });

    await descargarImagen();

}

async function descargarImagen(){
    name = document.getElementById('imgNombre').value;
    // import { getStorage, ref, getDownloadURL } from "firebase/storage";
    const storageRef = refS(storage, 'Imagenes/' + name);

    // Get the download URL
    await getDownloadURL(storageRef)
    .then((url) => {
        document.getElementById('url').value = url;
        document.getElementById('Preview').src = url;
    })
    .catch((error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
        case 'storage/object-not-found':
            console.log("No existe el archivo")
            break;
        case 'storage/unauthorized':
            console.log("No tiene permisos");
            break;
        case 'storage/canceled':
            console.log("Se cancelo o no tiene internet");
            break;

        // ...

        case 'storage/unknown':
            console.log("Error desconocido");
            break;
        }
    });

}




// Evento click
if(btnInsertar){
    btnInsertar.addEventListener('click', insertarDatos);
}

if(btnBuscar){
    btnBuscar.addEventListener('click', mostrarDatos);
}

if(btnActualizar){
    btnActualizar.addEventListener('click', actualizar);
}

if(btnBorrar){
    btnBorrar.addEventListener('click', borrar);
}

if(btnTodos){
    btnTodos.addEventListener('click', mostrarProductos);
}

if(btnLimpiar){
    btnLimpiar.addEventListener('click', limpiar);
}

if(archivo){
    archivo.addEventListener('change', cargarImagen);
}


if(btnMostrarImagen){
    btnMostrarImagen.addEventListener('click', descargarImagen)
}